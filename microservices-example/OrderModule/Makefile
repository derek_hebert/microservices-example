# Use Bash in this makefile.
SHELL := /bin/bash

# These commands do not refer to files. When given as a target, always run their recipes.
.PHONY: up stop down logs build clean ps

# Pass these options to every call to docker-compose.
# In particular, compose a single "virtual" docker-compose.yml from several.
# This allows each compontent to define how to build its services seperately,
# and then stitch them together into a single OrderModule.
DOCKER_COMPOSE_OPTS= \
		--project-name order-module \
		--file place-order-api/docker-compose.yml \
		--file approve-order-api/docker-compose.yml \
		--file docker-compose.yml

build:
	$(MAKE) --directory place-order-api build
	$(MAKE) --directory approve-order-api build

clean:
	$(MAKE) --directory place-order-api clean
	$(MAKE) --directory approve-order-api clean

down:
	docker-compose $(DOCKER_COMPOSE_OPTS) down --volumes

logs:
	docker-compose $(DOCKER_COMPOSE_OPTS) logs $(SERVICES)

ps:
	docker-compose $(DOCKER_COMPOSE_OPTS) ps --all

services:
	docker-compose $(DOCKER_COMPOSE_OPTS) ps --services

stop:
	docker-compose $(DOCKER_COMPOSE_OPTS) stop $(SERVICES)

up:
	docker-compose $(DOCKER_COMPOSE_OPTS) up --detach $(SERVICES)

rm:
	# -v removes any anonymous volumes attached to containers.
	docker-compose $(DOCKER_COMPOSE_OPTS) rm --force --stop -v $(SERVICES)
