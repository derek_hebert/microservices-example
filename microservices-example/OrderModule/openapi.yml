openapi: '3.0.2'
info:
  description: |
    Simplified version of the Bear Necessitities Market (LibreFoodPantry) order system.
  title: Order Module API
  version: '1.0.0'
  contact:
    name: Karl R. Wurst
    email: kwurst@worcester.edu
  license:
    name: GPLV3
    url: 'https://www.gnu.org/licenses/gpl-3.0.en.html'
servers:
  - url: http://localhost:10001/v1
tags:
  - name: place order
    description: Placing an order at BNM
  - name: approve order
    description: Approving (or not) an order at BNM
paths:
  /items:
    get:
      tags:
        - place order
      summary: List the items that can be ordered
      operationId: listItems
      parameters:
        - $ref: '#/components/parameters/SortParameter'
        - $ref: '#/components/parameters/LimitParameter'
        - $ref: '#/components/parameters/PagingParameter'
      responses:
        '200':
          description: OK
          content:
            application/json:
              schema:
                type: object
                properties:
                  items:
                    type: array
                    items: 
                      $ref: '#/components/schemas/Item'
                  pagingToken:
                    $ref: '#/components/schemas/PagingToken'
  /orders:
    post:
      tags:
        - place order
      summary: Place an order
      operationId: placeOrder
      requestBody:
        content:
          application/json:
            schema:
              $ref: '#/components/schemas/OrderRequest'
      responses:
        '201':
          description: Created
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/OrderResponse'
        '400':
          description: Bad Request
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/Error'
    get:
      tags:
        - place order
      summary: List all orders
      operationId: getOrders
      parameters:
        - $ref: '#/components/parameters/SortParameter'
        - $ref: '#/components/parameters/LimitParameter'
        - $ref: '#/components/parameters/PagingParameter'
      responses:
        '200':
          description: OK
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/OrderResponseList'
  /orders/{id}:
    get:
      tags:
        - place order
      summary: Get a specific order
      operationId: getOrder
      parameters:
        - $ref: '#/components/parameters/IdParameter'
      responses:
        '200':
          description: OK
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/OrderResponse'
        '400':
          description: Bad Request
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/Error'
        '404':
          description: Not Found
  /orders/approved:
    get:
      tags:
        - approve order
      summary: List all approved orders
      operationId: getApprovedOrders
      parameters:
        - $ref: '#/components/parameters/SortParameter'
        - $ref: '#/components/parameters/LimitParameter'
        - $ref: '#/components/parameters/PagingParameter'
      responses:
        '200':
          description: OK
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/OrderApprovalResponseList'
  /orders/cancelled:
    get:
      tags:
        - approve order
      summary: List all cancelled orders
      operationId: getCancelledOrders
      parameters:
        - $ref: '#/components/parameters/SortParameter'
        - $ref: '#/components/parameters/LimitParameter'
        - $ref: '#/components/parameters/PagingParameter'
      responses:
        '200':
          description: OK
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/OrderApprovalResponseList'
  /orders/unreviewed:
    get:
      tags:
        - approve order
      summary: List all unreviewed orders
      operationId: getUnreviewedOrders
      parameters:
        - $ref: '#/components/parameters/SortParameter'
        - $ref: '#/components/parameters/LimitParameter'
        - $ref: '#/components/parameters/PagingParameter'
      responses:
        '200':
          description: OK
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/OrderApprovalResponseList'
  /orders/{id}/approve:
    patch:
      tags:
        - approve order
      summary: Approve an order
      operationId: approveOrder
      parameters:
        - $ref: '#/components/parameters/IdParameter'
      responses:
        '200':
          description: OK
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/OrderApprovalResponse'
        '400':
          description: Bad Request
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/Error'
        '404':
          description: Not Found
  /orders/{id}/cancel:
    patch:
      tags:
        - approve order
      summary: cancel an order
      operationId: cancelOrder
      parameters:
        - $ref: '#/components/parameters/IdParameter'
      responses:
        '200':
          description: OK
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/OrderApprovalResponse'
        '400':
          description: Bad Request
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/Error'
        '404':
          description: Not Found
components:
  schemas:
    Email:
      type: string
      pattern: (?:[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*|"(?:[\x01-\x08\x0b\x0c\x0e-\x1f\x21\x23-\x5b\x5d-\x7f]|\\[\x01-\x09\x0b\x0c\x0e-\x7f])*")@(?:(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?|\[(?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?|[a-z0-9-]*[a-z0-9]:(?:[\x01-\x08\x0b\x0c\x0e-\x1f\x21-\x5a\x53-\x7f]|\\[\x01-\x09\x0b\x0c\x0e-\x7f])+)\])
    _Id:
      type: string
      pattern: ^[a-fA-F0-9]{24}$
      description: A MongoDB id - a 24 hex character string
    PagingToken:
      type: string
      pattern: ^[a-fA-F0-9]{24}$
      description: A MongoDB id - a 24 hex character string
    Item:
      type: object
      properties:
        _id:
          $ref: '#/components/schemas/_Id'
        name:
          type: string
    OrderRequest:
      type: object
      properties:
        email:
          $ref: '#/components/schemas/Email'
        itemsList:
          type: array
          items:
            $ref: '#/components/schemas/Item'
        restrictions:
          type: string
          maxLength: 1000
        preferences:
          type: string
          maxLength: 1000
    OrderResponse:
      type: object
      properties:
        _id:
          $ref: '#/components/schemas/_Id'
        email:
          $ref: '#/components/schemas/Email'
        itemsList:
          type: array
          items:
            $ref: '#/components/schemas/Item'
        restrictions:
          type: string
          maxLength: 1000
        preferences:
          type: string
          maxLength: 1000
    OrderResponseList:
      type: object
      properties:
        pagingToken:
          $ref: '#/components/schemas/PagingToken'
        orders:
          type: array
          items:
            $ref: '#/components/schemas/OrderResponse'
    OrderApprovalResponse:
      type: object
      properties:
        _id:
          type: string
        email:
          $ref: '#/components/schemas/Email'
        itemsList:
          type: array
          items:
            $ref: '#/components/schemas/Item'
        restrictions:
          type: string
          maxLength: 1000
        preferences:
          type: string
          maxLength: 1000
        reviewStatus:
          type: string
          enum: [ "unreviewed", "approved", "cancelled" ]
          default: "unreviewed"          
    OrderApprovalResponseList:
      type: object
      properties:
        pagingToken:
          $ref: '#/components/schemas/PagingToken'
        orders:
          type: array
          items:
            $ref: '#/components/schemas/OrderApprovalResponse'
    Error:
      type: object
      properties:
        httpResponseCode:
          type: integer
        messages:
          type: array
          items:
            type: string
  parameters:
    SortParameter:
      name: sortBy
      in: query
      description: Which field to sort by
      required: false
      schema:
        type: string
        enum: [ "-created", "+created" ]
        default: "-created"
    LimitParameter:
      name: limit
      in: query
      description: Number of items to return
      required: false
      schema:
        type: integer 
        minimum: 1
        maximum: 100
        default: 50
    PagingParameter:
      name: pagingToken
      in: query
      description: Used to specify next page
      required: false
      schema:
        $ref: '#/components/schemas/PagingToken'
    IdParameter:
      name: id
      in: path
      description: Identifies an order.
      required: true
      schema:
        $ref: '#/components/schemas/_Id'
