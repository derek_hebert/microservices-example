# Microservices Example

## Problem

One or more teams wants to learn how to build microservice web-application using Docker, which is the chosen architecture for LibreFoodPantry.

## Solution

Teams work through a set of activities that explore a small, but realistic example of web-application implemented using a microservice architecture. The activities and the accompanying example are organized into separate projects.

- [Microservices activities project](https://gitlab.com/LibreFoodPantry/training/microservices-activities)
- [Microservices example project](https://gitlab.com/LibreFoodPantry/training/microservices-example)

## Why Microservices?

LibreFoodPanty has adopted the microservices architecture because it makes it easier for different teams to

- work on smaller, easier to understand modules
- develop code in whatever language or with whatever tools they are already comfortable
- redesign/redevelop modules to meet the needs of their food pantry
- work on frontend or backend code based on interest

## The OrderModule Example

This example is demonstrates how to design a REST API, implement the REST API as a backend server with a persistence layer using a document database, develop and implement a frontend that interacts with the user and calls the backend through the REST API.

The example is a simplified version of Bear Necessities Market’s ordering system. Bear Necessities Market (BNM) is a member project of LibreFoodPantry. The example ordering system will use a smaller set of features than BNM – just enough to support the learning goals of the activities, while still giving the feel of a larger system.

The working code examples given for participants to explore, modify, and extend will be containerized in the same way that LFP services are being containerized in Docker.

## Setup

You're going to need the following tools to work with this example:

- Bash
- Git
- Docker Desktop for building Linux containers
- IDE: We recomend VS Codium (or VS Code). It's open-source, cross-platform, supports multiple languages, and is modern.

The instructions that follow provide a single way to get setup for select operating systems. They are not intended to be comprehensive. And things change over time. Be prepared to read other documentation and troubleshoot problems. These instructions are based on the authors' experience. If you know what you are doing, you may deviate from these instructions. Otherwise we recommend following along.

If you find a problem with these instructions, please report it by [opening an issue]().

## Setup for MacOS

### Install Homebrew for MacOS

[Follow Homebrew's install instructions.](https://brew.sh/)

### Bash on MacOS

MacOS comes with Bash, but that version supports the scripts that are used to manage your system. When you upgrade your Mac, Apple will upgrade this version as necessary. So you should really install your own version of Bash that you have control over.

```bash
brew install bash
```

Close terminal and open a new terminal. Check your bash version.

```bash
bash --version
```

Should be 5.0 or higher.

### Git on MacOS

```bash
brew install git
git --version
```

Should be 2.28 or higher.

### Install Docker Desktop for MacOS

```bash
brew cask install docker
open /Applications/Docker.app
```

### Install VS Codium (or VS Code)

VS Codium is the open-source distribution of VS Code. They are build from the same open-source code-base. VS Code is branded with Microsoft and is distributed under a non-open-source license. Ultimately, you'll get the same IDE either way. So why not go open-source?!

```bash
brew cask isntall vscodium
open /Applications/VSCodium.app
```

## Setup for Windows 10

### Bash and Git for Windows

[Follow directions from Git-SCM.](https://git-scm.com/)

In all instructions activities, we'll assume you are in a Bash shell.

### Docker Desktop for Windows 10 ***Pro, Enterprise, or Education*** for building Linux containers

[Follow instructions on this site.](https://docs.docker.com/docker-for-windows/install/)

If you have any troubles, you may want to read some of the steps in Docker Desktop for Windows 10 ***Home***, as they are based on one of the authors' experience which might give you a clue to fix your problem.

### Docker Desktop for Windows 10 ***Home***

1. Enable virtualization in your BIOS.
    - [Check if virtualization is enabled.](https://thegeekpage.com/how-to-check-if-virtualization-is-enabled-in-windows-10/)
    - [Enable virtualizeion in BIOS.](https://2nwiki.2n.cz/pages/viewpage.action?pageId=75202968#:~:text=Press%20F2%20key%20at%20startup,and%20then%20press%20the%20Enterkey.)
2. Update to Windows 10, version **2004** or higher.
    - [Check your Windows' version.](https://support.microsoft.com/en-us/help/13443/windows-which-version-am-i-running)
    - Use Windows Updator to install all updates availabe for your system.
    - [Check your Windows' version.](https://support.microsoft.com/en-us/help/13443/windows-which-version-am-i-running)
    - If you still don't have version **2004** or higher, [use Microsoft Update Assistant](https://support.microsoft.com/en-us/help/3159635/windows-10-update-assistant) to manually install the latest version of Windows 10.
3. [Install and enable WSL 2.](https://docs.microsoft.com/en-us/windows/wsl/install-win10)
    - If you get an error when you run `wsl --set-default-version 2`, [update the wsl kernel](https://aka.ms/wsl2kernel).
4. [Install Docker Desktop for Windows 10 Home.](https://docs.docker.com/docker-for-windows/install-windows-home/)
5. Add this to the `.bashrc` in your home directory (create it if it doesn't exist).
    ```bash
    # From https://github.com/rprichard/winpty/issues/125#issuecomment-473446719
    docker() {
        realdocker='/c/Program Files/Docker/Docker/Resources/bin/docker'
        export MSYS_NO_PATHCONV=1 MSYS2_ARG_CONV_EXCL="*"
        printf "%s\0" "$@" > /tmp/args.txt
        winpty bash -c "xargs -0a /tmp/args.txt '$realdocker'"
    }
    ```

## Getting Started as a Developer

Start Docker Desktop if it is not already running.

Open a terminal (on Mac) or git-bash (on Windows), and then clone this project and change into the root directory of the project. 

```bash
git clone https://gitlab.com/LibreFoodPantry/training/microservices-example.git
cd microservices-examples
```

Build and run the development shell.

```bash
shell/run.sh
```

The first time you run this, it will take some time to build the shell. Subsequent runs will be much faster.

When it's done, you will be running inside a Docker container that has a number of development tools pre-installed.

> With Docker, we can create a shell that all of the developers on a project use pre-installed with all of the tools needed to build, run, and maintain the project. This ensures that the tools used to develop on a project are consistent across all developers on a team, regardless what operating system they are running!

Currently there are some limitations:

- You cannot run git from inside the development shell. You might want to open a second terminal for manage git.
- You cannot run curl to test the API from within the shell. This is because the shell is not on the same network as the services' containers, and localhost doesn't refer to the host machine (it refers to the container that you are running inside).


## Build System

We use GNU Make to manage the build process. It in turn uses command-line tools through a a bash shell. standard and custom shell commands, Docker, and Docker-compose


Learn how to build, run, and stop the system.

```bash
make help
```

## Build system limitations

Currently you cannot use git while you are inside the development shell.

As you develop and restart things, Docker creates and manages various resources. Some of these you no longer need. So at the end of a development session, I like to delete everything that Docker has created. This is impractical in a production environment, but makes sense in a development environment. The instructions below delete ***EVERYTHING*** that Docker has created for you. Use them with care.

```bash
# Stop ALL processes.
docker stop $(docker ps -a -q)

# Remove ALL containers; must be stopped first.
docker rm $(docker ps -a -q)

# Remove ALL volumes, networks, and orphaned images.
docker system prune -a -f --volumes

# Remove ALL images.
docker rmi $(docker images -a -q)
```

## More About the Build Process

If you want to learn more about the build process and how to create new endpoints for services,
see [Build Process](BuildProcess.md).